/************************************************************//**
 * @file   main.c                                      		*                                                  
 * @brief Header function for the needs of cs-240a project 2017 *
 ***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "troy.h"

#define BUFFER_SIZE 1024  /**< Maximum length of a line in input file */

/* Uncomment the following line to enable debugging prints 
 * or comment to disable it */
//#define DEBUG

#ifdef DEBUG
#define DPRINT(...) fprintf(stderr, __VA_ARGS__);
#else  /* DEBUG */
#define DPRINT(...)
#endif /* DEBUG */


	/**
	* @brief Optional function to initialize data structures that 
	* need initialization
	*
	* @return 1 on success
	*         0 on failure
	*/
	int initialize() {
		//Ftiaxnoume tous komvous frourous
		registration_list = (struct soldier*)malloc(sizeof(struct soldier));
		if (registration_list == 0) {
			printf("ERROR: Out of memory\n");
			return 0;
		}
		registration_sentinel = registration_list;
		registration_sentinel->sid = -1;
		registration_sentinel->gid = -1;
		registration_sentinel->next = NULL;

		generals_list = (struct general*)malloc(sizeof(struct general));
		if (generals_list == 0) {
			printf("ERROR: Out of memory\n");
			return 0;
		}
		general_sentinel = generals_list;
		general_sentinel->gid = -1;
		general_sentinel->combats_no = -1;
		general_sentinel->soldiers_head = NULL;
		general_sentinel->soldiers_tail = NULL;
		general_sentinel->next = NULL;
		return 1;
    	return 1;
    }
        
	/**
	* @brief Register soldier
	*
	* @param sid The soldier's id
    * @param gid The general's id who orders the soldier
	*
	* @return 1 on success
	*         0 on failure
	*/
	int register_soldier(int sid, int gid) {

		struct soldier *p;
		p = (struct soldier*)malloc(sizeof(struct soldier));
		if (p == 0) {
			printf("ERROR: Out of memory\n");
			return 0;
		}
		p->sid = sid;
		p->gid = gid;
		p->next = registration_list;
		registration_list = p;
		return 1;
	}

	/**
	 * @brief General or King joins the battlefield
	 *
     * @param gid The general's id
	 * @return 1 on success
	 *         0 on failure
	 */
	int add_general(int gid) {

		struct general *p;
		p = (struct general*)malloc(sizeof(struct general));
		if (p == 0) {
			printf("ERROR: Out of memory\n");
			return 0;
		}
		p->gid = gid;
		p->next = generals_list;
		generals_list = p;
		return 1;
	}
	

	/**
	 * @brief Distribute soldiers to the camps
     * 
	 * @return 1 on success
	 *         0 on failure
	 */
	int distribute() {
		struct soldier *r = registration_list;
		struct general *g = generals_list;
		struct DDL_soldier *DDL, *tmp;

		while (r->gid != -1) {
			DDL = (struct DDL_soldier *)malloc(sizeof(struct DDL_soldier));
			if (!DDL) {
				printf("ERROR: Out of memory\n");
				return 0;
			}
			DDL->sid = r->sid;
			DDL->gid = r->gid;

			while (g->gid != -1) {
				if (g->gid == DDL->gid) {

					//an einai o prwtos komvos pou mpainei
					if (g->soldiers_head == NULL) {
						g->soldiers_head = DDL;
						g->soldiers_tail = DDL;
						break;
					}
					tmp = g->soldiers_head; //deixnei stin arxh tis listas
											//an einai o mikroteros kai mpainei stin arxh
					if (DDL->sid < tmp->sid) {
						DDL->next = tmp;
						DDL->prev = NULL;
						tmp->prev = DDL;
						g->soldiers_head = DDL;
						break;
					}
					else {
						//an o komvos exei to megalytero sid kai mpainei sto telos
						if (DDL->sid > g->soldiers_tail->sid) {
							DDL->prev = g->soldiers_tail;
							DDL->next = NULL;
							(g->soldiers_tail)->next = DDL;
							g->soldiers_tail = DDL;
							break;
						}
						//an mpainei kapou endiamesa
						else {
							while (DDL->sid > tmp->sid) {
								tmp = tmp->next;
							}
							DDL->prev = tmp->prev;
							DDL->next = tmp;
							tmp->prev = DDL;
							(DDL->prev)->next = DDL;
							break;
						}
					}
				}
				g = g->next;
			}
			g = generals_list;
			r = r->next;
		}
		printf("\nD\n\tGENERALS:\n");
		while (g->gid != -1) {
			printf("\n\t<%d>:", g->gid);
			DDL = g->soldiers_head;
			while (DDL != NULL) {
				printf(" <%d> ", DDL->sid);
				DDL = DDL->next;
			}
			g = g->next;
		}
		printf("\nDONE\n");
		return 1;
	}

	void ListTransport(struct DDL_soldier *mirmidones, struct general *g) {

		struct DDL_soldier *tmp1, *tmp2;
		tmp1 = mirmidones;
		mirmidones = mirmidones->next;
		tmp2 = g->soldiers_head;

		while (mirmidones != NULL) {
			//an o mirmidonas exei to mikrotero sid
			if (tmp2->sid > tmp1->sid) {
				tmp1->next = tmp2;
				tmp2->prev = tmp1;
				g->soldiers_head = tmp1;
				tmp1->gid = tmp2->gid;
				tmp1 = mirmidones;
				mirmidones = mirmidones->next;
			}
			else {
				while (tmp2->sid < tmp1->sid) {
					//an o mirmidonas exei to megalitero sid
					if (tmp2->next == NULL) {
						tmp1->prev = tmp2;
						tmp2->next = tmp1;
						g->soldiers_tail = tmp1;
						tmp1->gid = tmp2->gid;
						tmp2 = tmp1;
						tmp1 = mirmidones;
						mirmidones = mirmidones->next;
						break;
					}
					tmp2 = tmp2->next;
				}
				if (g->soldiers_tail != tmp1) {
					tmp1->next = tmp2;
					tmp1->prev = tmp2->prev;
					tmp2->prev = tmp1;
					tmp1->prev->next = tmp1;
					tmp1->gid = tmp2->gid; //ananewnw to anagnoristiko
					tmp2 = tmp1;
					tmp1 = mirmidones;
					mirmidones = mirmidones->next;
				}
			}
		}
		//to tmp1 krata tin timi tou teleftaiou mirmidona
		while (tmp2->sid < tmp1->sid) {
			if (tmp2->next == NULL) {
				tmp1->prev = tmp2;
				tmp2->next = tmp1;
				g->soldiers_tail = tmp1;
				tmp1->gid = tmp2->gid;
				break;
			}
			tmp2 = tmp2->next;
		}
		if (g->soldiers_tail != tmp1) {
			tmp1->next = tmp2;
			tmp1->prev = tmp2->prev;
			tmp2->prev = tmp1;
			tmp1->prev->next = tmp1;
			tmp1->gid = tmp2->gid;
			tmp2 = tmp1;
		}
	}

	/**
	 * @brief General resigns from battlefield
	 *
     * @param gid1 The id of general that resigns
     * @param gid2 The id of the general that will  
     * 
	 * @return 1 on success
	 *         0 on failure
	 */
	int general_resign(int gid1, int gid2) {

		struct general *achilles, *g = generals_list;
		struct DDL_soldier *mirmidones;
		while (g->gid != -1) {
			if ((g->next)->gid == gid1) {
				achilles = g->next;
				g->next = achilles->next;
				achilles->next = NULL;
				mirmidones = achilles->soldiers_head;
				achilles->soldiers_head = NULL;
				achilles->soldiers_tail = NULL;
				free(achilles);
				break;
			}
			g = g->next;
		}
		g = generals_list;
		while (g->gid != -1) {
			if (g->gid == gid2) {
				ListTransport(mirmidones, g);
				break;
			}
			g = g->next;
		}
		struct DDL_soldier *DDL;
		g = generals_list;
		printf("\nM\n\tGENERALS:\n");
		while (g->gid != -1) {
			printf("\n\t<%d>:", g->gid);
			DDL = g->soldiers_head;
			while (DDL != NULL) {
				printf(" <%d> ", DDL->sid);
				DDL = DDL->next;
			}
			g = g->next;
		}
		printf("\nDONE\n");
		return 1;
	}

	void InsertCombatSoldier(struct DDL_soldier *tmp, struct c_soldier **last) {

		struct c_soldier *c;
		c = (struct c_soldier *)malloc(sizeof(struct c_soldier));
		if (!c) {
			printf("ERROR: Out of memory\n");
			return;
		}
		c->sid = tmp->sid;
		c->gid = tmp->gid;
		c->alive = 1;
		c->next = NULL;
		if (my_combat.soldier_cnt == 0) {
			my_combat.combat_s = c;
			*last = my_combat.combat_s;
			(my_combat.soldier_cnt)++;
			return;
		}
		(*last)->next = c;
		*last = (*last)->next;
		(my_combat.soldier_cnt)++;
	}
        
	/**
	 * @brief Prepare for battle
     * 
	 * @param gid1 The id of the first general
     * @param gid2 The id of the second general
     * @param gid3 The id of the third general
      * 
	 * @return 1 on success
	 *         0 on failure
	 */
	int prepare_battle(int gid1, int gid2, int gid3) {

		struct general *g = generals_list;
		struct c_soldier *last;
		struct DDL_soldier *front1, *front2, *front3;
		struct DDL_soldier *back1, *back2, *back3;

		while (g->gid != -1) {
			if (g->gid == gid1) {
				front1 = g->soldiers_head;
				back1 = g->soldiers_tail;
				(g->combats_no)++;
			}
			if (g->gid == gid2) {
				front2 = g->soldiers_head;
				back2 = g->soldiers_tail;
				(g->combats_no++);
			}
			if (g->gid == gid3) {
				front3 = g->soldiers_head;
				back3 = g->soldiers_tail;
				(g->combats_no)++;
			}
			g = g->next;
		}

		while (front1 != back1->next || front2 != back2->next || front3 != back3->next) {
			if (front1 != back1->next) {
				InsertCombatSoldier(front1, &last);
				front1 = front1->next;
			}
			if (front2 != back2->next) {
				InsertCombatSoldier(front2, &last);
				front2 = front2->next;
			}
			if (front3 != back3->next) {
				InsertCombatSoldier(front3, &last);
				front3 = front3->next;
			}
			if (front1 != back1->next) {
				InsertCombatSoldier(back1, &last);
				back1 = back1->prev;
			}
			if (front2 != back2->next) {
				InsertCombatSoldier(back2, &last);
				back2 = back2->prev;
			}
			if (front3 != back3->next) {
				InsertCombatSoldier(back3, &last);
				back3 = back3->prev;
			}
		}
		return 1;
	}
        
	/**
	 * @brief Battle
     * 
     * @param god_favor 
     *
	 * @return 1 on success
	 *         0 on failure
	 */
	int battle(int god_favor) {

		int i, xaros = 0, to_die = 0;
		struct c_soldier *c = my_combat.combat_s;
		if (god_favor == 0) {
			to_die = 0.4*my_combat.soldier_cnt;
			while (xaros != to_die + 1) {
				c->alive = 0;
				c = c->next;
				xaros++;
			}
		}
		if (god_favor == 1) {
			while (c != NULL) {
				c->alive = 0;
				for (i = 0; i < 10; i++) {
					c = c->next;
					if (c == NULL) break;
				}
			}
		}
		printf("\nB %d", god_favor);
		printf("\n\tCombat soldiers: ");
		c = my_combat.combat_s;
		while (c != NULL) {
			printf("<%d:%d> ", c->sid, c->alive);
			c = c->next;
		}
		printf("\nDONE\n");
		return 1;
	}

	void RemoveSoldier(struct general *g, struct c_soldier *c, struct DDL_soldier *front, struct DDL_soldier *back) {
		if (c->sid == front->sid) {
			if (front == g->soldiers_head) {
				front = front->next;
				g->soldiers_head = front;
			}
			else {
				(front->next)->prev = front->prev;
				(front->prev)->next = front->next;
				front = front->next;
			}
		}
		else if (c->sid == back->sid) {
			if (back == g->soldiers_tail) {
				back = back->prev;
				g->soldiers_tail = back;
			}
			else {
				(back->prev)->next = back->next;
				(back->next)->prev = back->prev;
				back = back->prev;
			}
		} /*
		  else {
		  printf("\nERROR-REMOVE %d", c->sid);
		  } */
	}
	void Next(struct c_soldier *c, struct DDL_soldier *front, struct DDL_soldier *back) {
		if (c->sid == front->sid) {
			front = front->next;
			return;
		}
		if (c->sid == back->sid) {
			back = back->prev;
			return;
		} /*
		  printf("\nERROR-NEXT %d", c->sid);
		  */
	}
        
	/**
	 * @brief Cease fire
     * 
	 * @return 1 on success
	 *         0 on failure
	 */
	int cease_fire() {

		struct general *g = generals_list;
		struct c_soldier *c = my_combat.combat_s;
		struct DDL_soldier *front1, *front2, *front3;
		struct DDL_soldier *back1, *back2, *back3;
		struct soldier *r = registration_list;
		int gid1 = -1, gid2 = -1, gid3 = -1;
		int count = 1;

		while (c != NULL) { //diatrexw tin combat list
			while (g->gid != c->gid) { //diatrexw tin generals list mexri na vrw se poio stratigo anikei o twrinos c_soldier
				g = g->next;
			}
			//----------------------------------------------------------------------------------------------------------------
			//tin prwti fora vazw tous deiktes front1 kai back1 sti DDL_soldier list
			if (count == 1) {
				front1 = g->soldiers_head;
				back1 = g->soldiers_tail;
				gid1 = front1->gid;
			}
			//an vriskomai sto strato tou 1ou stratigou
			if (c->gid == gid1) {
				//kai o stratiotis einai nekros
				if (c->alive == 0) {
					RemoveSoldier(g, c, front1, back1); //ton afairw apo tin DDL_soldier list
					r = registration_list;
					while (r->sid != -1) {
						if (r->next->sid == c->sid) {
							r->next = r->next->next;
						}
						r = r->next;
					}
				}
				else {
					Next(c, front1, back1); //alliws o deiktis na deixnei ston epomeno
				}
			}
			//----------------------------------------------------------------------------------------------------------------
			//tin prwti fora vazw tous deiktes front2 kai back2 sti DDL_soldier list
			if (count == 2) {
				front2 = g->soldiers_head;
				back2 = g->soldiers_tail;
				gid2 = front2->gid;
			}
			//an vriskomai sto strato tou 2ou stratigou
			if (c->gid == gid2) {
				//kai o stratiotis einai nekros
				if (c->alive == 0) {
					RemoveSoldier(g, c, front2, back2); //ton afairw
					r = registration_list;
					while (r->sid != -1) {
						if (r->next->sid == c->sid) {
							r->next = r->next->next;
						}
						r = r->next;
					}
				}
				else {
					Next(c, front2, back2); //alliws o deiktis na deixnei ston epomeno
				}
			}
			//----------------------------------------------------------------------------------------------------------------
			//tin prwti fora vazw tous deiktes front3 kai back3 sti DDL_soldier list
			if (count == 3) {
				front3 = g->soldiers_head;
				back3 = g->soldiers_tail;
				gid3 = front3->gid;
			}
			//an vriskomai sto strato tou 2ou stratigou
			if (c->gid == gid3) {
				//kai o stratiotis einai nekros
				if (c->alive == 0) {
					RemoveSoldier(g, c, front3, back3); //ton afairw
					r = registration_list;
					while (r->sid != -1) {
						if (r->next->sid == c->sid) {
							r->next = r->next->next;
						}
						r = r->next;
					}
				}
				else {
					Next(c, front3, back3); //alliws o deiktis na deixnei ston epomeno
				}
			}
			count++;
			g = generals_list;
			//sto telos afairw ton stratioti apo tin combat list
			my_combat.combat_s = c->next;
			c->next = NULL;
			c = my_combat.combat_s;
			(my_combat.soldier_cnt)--;
		}
		//g = generals_list;
		struct DDL_soldier *DDL;
		printf("\nU\n\tGENERALS:\n");
		while (g->gid != -1) {
			printf("\n\t<%d>:", g->gid);
			DDL = g->soldiers_head;
			while (DDL != NULL) {
				printf(" <%d> ", DDL->sid);
				DDL = DDL->next;
			}
			g = g->next;
		}
		printf("\nDONE\n");
		return 1;
	}
        
	/**
	 * @brief Trojan Horse
	 *
	 * @return 1 on success
	 *         0 on failure
	 */
	int trojan_horse() {

		struct general *stratigos[5];
		struct general *g = generals_list;
		int i, min;
		for (i = 0; i < 5; i++) {
			if (i == 0) {
				stratigos[i] = g;
				min = g->combats_no;
			}
			else {
				stratigos[i] = g;
				if (g->combats_no < min) {
					min = g->combats_no;
				}
			}
			g = g->next;
		}
		while (g->gid != -1) {
			if (g->combats_no > min) {
				for (i = 0; i < 5; i++) {
					if (stratigos[i]->combats_no == min) {
						stratigos[i] = g;
						break;
					}
				}
				for (i = 0; i < 5; i++) {
					if (stratigos[i]->combats_no < min) {
						min = stratigos[i]->combats_no;
						break;
					}
				}
			}
			g = g->next;
		}
		printf("\nT\n\tGeneral = ");
		for (i = 0; i < 5; i++) {
			printf("<%d:%d> ", stratigos[i]->gid, stratigos[i]->combats_no);
		}
		printf("\nDONE\n");
		return 1;
	}
        
	/**
	 * @brief Print generals
	 *
	 * @return 1 on success
	 *         0 on failure
	 */
	int print_generals() {
		struct general *tmpg = generals_list;
		struct DDL_soldier *tmpr;
		printf("\nX\n\tGENERALS:\n");
		while (tmpg->gid != -1) {
			printf("\n\t<%d>:", tmpg->gid);
			tmpr = tmpg->soldiers_head;
			while (tmpr != NULL) {
				printf(" <%d> ", tmpr->sid);
				tmpr = tmpr->next;
			}
			tmpg = tmpg->next;
		}
		printf("\nDONE\n");
		return 1;
	}
        
	/**
	 * @brief Print registration list
	 *
	 * @return 1 on success
	 *         0 on failure
	 */
	int print_registration_list() {
		printf("\nY\n\tRegistration list = ");
		struct soldier *s = registration_list;
		while (s->sid != -1) {
			printf(" <%d:%d> ", s->sid, s->gid);
			s = s->next;
		}
		printf("\nDONE\n");
		return 1;
	}

	int free_world() {
		return 1;
	}

/**
 * @brief The main function
 *
 * @param argc Number of arguments
 * @param argv Argument vector
 *
 * @return 0 on success
 *         1 on failure
 */
int main(int argc, char** argv)
{
	FILE *fin = NULL;
	char buff[BUFFER_SIZE], event;

	/* Check command buff arguments */
	if ( argc != 2 ) {
		fprintf(stderr, "Usage: %s <input_file> \n", argv[0]);
		return EXIT_FAILURE;
	}

	/* Open input file */
	if (( fin = fopen(argv[1], "r") ) == NULL ) {
		fprintf(stderr, "\n Could not open file: %s\n", argv[1]);
		perror("Opening test file\n");
		return EXIT_FAILURE;
	}

	initialize();

	/* Read input file buff-by-buff and handle the events */
	while ( fgets(buff, BUFFER_SIZE, fin) ) {

		DPRINT("\n>>> Event: %s", buff);

		switch(buff[0]) {

		/* Comment */
		case '#':
			break;

		/* Register soldier
		 * R <sid> <gid> */
		case 'R':
		{
			int sid, gid;
			sscanf(buff, "%c %d %d", &event, &sid, &gid);
			DPRINT("%c %d %d\n", event, sid, gid);

			if (register_soldier(sid, gid)) {
				DPRINT("R %d %d succeeded\n", sid, gid);
			}
			else {
				fprintf(stderr, "R %d %d failed\n", sid, gid);
			}
			struct soldier *r = registration_list;
			printf("\nR %d %d", sid, gid);
			printf("\n\tRegistration list: ");
			while (r->sid != -1) {
				printf("<%d:%d> ", r->sid, r->gid);
				r = r->next;
			}
			printf("\nDONE\n");
			break;
		}

		/* General or king joins the battlefield
		 * G <gid> */
		case 'G':
		{
			int gid;
			sscanf(buff, "%c %d", &event, &gid);
			DPRINT("%c %d\n", event, gid);

			if (add_general(gid)) {
				DPRINT("%c %d succeeded\n", event, gid);
			}
			else {
				fprintf(stderr, "%c %d failed\n", event, gid);
			}

			struct general *g = generals_list;
			printf("\nG %d", gid);
			printf("\n\tGenerals: ");
			while (g->gid != -1) {
				printf("<%d> ", g->gid);
				g = g->next;
			}
			printf("\nDONE\n");
			break;
		}

		/* Distribute soldier
		 * D */
		case 'D':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (distribute()) {
				DPRINT("%c succeeded\n", event);
			}
			else {
				fprintf(stderr, "%c failed\n", event);
			}
			break;
		}

		/* General resigns from battle
		 * M <gid1> <gid2> */
		case 'M':
		{
			int gid1, gid2;

			sscanf(buff, "%c %d %d", &event, &gid1, &gid2);
			DPRINT("%c %d %d\n", event, gid1, gid2);

			if (general_resign(gid1, gid2)) {
				DPRINT("%c %d %d succeeded\n", event, gid1, gid2);
			}
			else {
				fprintf(stderr, "%c %d %d failed\n", event, gid1, gid2);
			}
			break;
		}

		/* Prepare for battle
		 * P <gid1> <gid2> <gid3> */
		case 'P':
		{
			int gid1, gid2, gid3;
			sscanf(buff, "%c %d %d %d", &event, &gid1, &gid2, &gid3);
			DPRINT("%c %d %d %d\n", event, gid1, gid2, gid3);

			if (prepare_battle(gid1, gid2, gid3)) {
				DPRINT("%c %d %d %d succeeded\n", event, gid1, gid2, gid3);

				struct c_soldier *c = my_combat.combat_s;
				printf("\nP %d %d %d", gid1, gid2, gid3);
				printf("\n\tCombat soldiers: ");
				while (c != NULL) {
					printf("<%d> ", c->sid);
					c = c->next;
				}
				printf("\nDONE\n");
			}
			else {
				fprintf(stderr, "%c %d %d %d failed\n", event, gid1, gid2, gid3);
			}
			break;
		}

		/* Battle
		 * B <god_favor> */
		case 'B':
		{
			int god_favor;
			sscanf(buff, "%c %d\n", &event, &god_favor);
			DPRINT("%c %d\n", event, god_favor);

			if (battle(god_favor)) {
				DPRINT("%c %d succeeded\n", event, god_favor);
			}
			else {
				fprintf(stderr, "%c %d failed\n", event, god_favor);
			}
			break;
		}

		/* Cease fire
         * U */
		case 'U':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if ( cease_fire() ) {
				DPRINT("%c succeeded\n", event);
			} else {
				fprintf(stderr, "%c failed\n", event);
			}
			break;
		}

		/* Trojan Horse
		 * T */
		case 'T':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if ( trojan_horse() ) {
				DPRINT("%c succeeded\n", event);
			} else {
				fprintf(stderr, "%c failed\n", event);
			}
			break;
		}

		/* Print generals
		 * X */
		case 'X':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (print_generals()) {
				DPRINT("%c succeeded\n", event);
			} else {
				fprintf(stderr, "%c failed\n", event);
			}
			break;
		}

		/* Print registration list
		 * Y */
		case 'Y':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (print_registration_list()) {
				DPRINT("%c succeeded\n", event);
			} else {
				fprintf(stderr, "%c failed\n", event);
			}
			break;
		}

		/* Empty line */
		case '\n':
			break;

		/* Ignore everything else */
		default:
			DPRINT("Ignoring buff: %s \n", buff);

			break;
		}
	}

	free_world();
	return (EXIT_SUCCESS);
}
