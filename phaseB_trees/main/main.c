﻿/************************************************************//**
 * @file   main.c                                      		*
 * @brief Main function for the needs of cs-240b project 2017   *
 ***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "troy.h"

#define BUFFER_SIZE 1024  /**< Maximum length of a line in input file */
#define a 7

 /* Uncomment the following line to enable debugging prints
  * or comment to disable it */
#define DEBUG

#ifdef DEBUG
#define DPRINT(...) fprintf(stderr, __VA_ARGS__);
#else  /* DEBUG */
#define DPRINT(...)
#endif /* DEBUG */

int isPrime(int n) {
	int i;
	for (i = 2; i <= n / 2; i++) {
		if (n % i == 0) return 0;
	}
	return 1;
}

int NextPrime(int n) {
	int nextPrime = n;
	int found = 0;

	while (!found) {
		nextPrime++;
		if (isPrime(nextPrime))
			found = 1;
	}
	return nextPrime;
}

/**
 * @brief Optional function to initialize data structures that
 * need initialization
 *
 * @return 1 on success
 *         0 on failure
 */

int N;

int initialize() {
	//Edw ftiaxnetai o pinakas katakermatismou
	N = NextPrime(max_soldiers_g / a);
	registration_hashtable = (struct soldier**)malloc(N * sizeof(struct soldier*));
	int i;
	for (i = 0; i < N; i++) {
		registration_hashtable[i] = NULL;
	}
	return 1;
}

/**
 * @brief Register soldier
 *
 * @param sid The soldier's id
 * @param gid The general's id who orders the soldier
 *
 * @return 1 on success
 *         0 on failure
 */
int register_soldier(int sid, int gid) {
	struct soldier *p, *tmp = NULL;
	int bucket; //h(k)=k mod Ν , opou k to sid
	bucket = sid % N;

	struct soldier *new = (struct soldier*)malloc(sizeof(struct soldier));
	if (new == 0) {
		printf("ERROR: Out of memory\n");
		return 0;
	}
	new->sid = sid;
	new->gid = gid;
	new->next = NULL;

	//an i lista einai keni
	if (registration_hashtable[bucket] == NULL) {
		registration_hashtable[bucket] = new;
	}
	else { //an exei hdh stoixeia
		new->next = registration_hashtable[bucket];
		registration_hashtable[bucket] = new;
		//printf("\n\nEverything OK\n\n");
	}
	int i;
	printf("\nR %d %d\n\tSoldiers Hash Table:\n\t", sid, gid);
	for (i = 0; i < N; i++) {
		tmp = registration_hashtable[i];
		while (tmp != NULL) {
			printf("<%d:%d> ", tmp->sid, tmp->gid);
			tmp = tmp->next;
		}
		printf("\n\t");
	}
	printf("\nDONE\n");
	return 1;
}

/**
 * @brief General or King joins the battlefield
 *
 * @param gid The general's id
 *
 * @return 1 on success
 *         0 on failure
 */
int add_general(int gid) {

	struct general *p;
	p = (struct general*)malloc(sizeof(struct general));
	if (p == 0) {
		printf("ERROR: Out of memory\n");
		return 0;
	}
	p->gid = gid;
	p->soldiers_S = (struct TREE_soldier*)malloc(sizeof(struct TREE_soldier));
	p->soldiers_S->sid = -1;
	p->soldiers_S->p = NULL;
	p->soldiers_S->lc = NULL;
	p->soldiers_S->rc = NULL;

	p->soldiers_R = NULL;
	p->next = generals_list;
	generals_list = p;

	struct general *g = generals_list;
	printf("\nG %d", gid);
	printf("\n\tGenerals: ");
	while (g != NULL) {
		printf("<%d> ", g->gid);
		g = g->next;
	}
	printf("\nDONE\n");
	return 1;
}

/* Eisagwgi se taksinomimeno dentro me komvo frouro */
struct TREE_soldier* BinaryTreeInsert(int sid, struct general *g) {
	struct TREE_soldier *tmp;
	tmp = (struct TREE_soldier*)malloc(sizeof(struct TREE_soldier));
	tmp->sid = sid;
	tmp->rc = g->soldiers_S;
	tmp->lc = g->soldiers_S;
	if (g->soldiers_R == NULL) {
		tmp->p = NULL;
		return tmp;
	}
	struct TREE_soldier *q, *p;
	q = g->soldiers_R;
	while (q->sid != -1) {
		p = q;
		if (sid < (q->sid))q = q->lc;
		else q = q->rc;
	}
	if (sid < (p->sid)) p->lc = tmp;
	else p->rc = tmp;
	tmp->p = p;
	return g->soldiers_R;
}

void InOrder(struct TREE_soldier *p) {
	if (p->sid == -1) return;
	InOrder(p->lc);
	printf("<%d> ", p->sid);
	InOrder(p->rc);
}

/**
 * @brief Distribute soldiers to the camps
 *
 * @return 1 on success
 *         0 on failure
 */
int distribute() {

	struct soldier *r;
	struct general *g;
	int i;
	for (i = 0; i < N; i++) {
		r = registration_hashtable[i];
		while (r != NULL) {
			g = generals_list;
			while (g != NULL) {
				if (g->gid == r->gid) {
					g->soldiers_no++;
					g->soldiers_R = BinaryTreeInsert(r->sid, g);
					break;
				}
				g = g->next;
			}
			r = r->next;
		}
	}
	print_generals();
	return 1;
}

void PreOrder(struct TREE_soldier *p, int *count) {
	if (p == NULL) return;
	(*count)++;
	PreOrder(p->lc, count);
	PreOrder(p->rc, count);
}

int campsize(int gid) {
	struct general *g = generals_list;
	int count = 0;
	while (g != NULL) {
		if (g->gid == gid) {
			PreOrder(g->soldiers_R, &count);
			//Visit: count nodes
		}
		g = g->next;
	}
	return count;
}

/* Epistrefei ton general me afto to gid */
struct general* find_general(int gid) {
	struct general *g = generals_list;
	while (g != NULL) {
		if (g->gid == gid) {
			return g;
		}
		g = g->next;
	}
	return NULL;
}

int i = 0; // metraei tis theseis tou voithitikou pinaka
void ArrayInsert(int *array, struct TREE_soldier *T) {
	struct TREE_soldier *p = T;
	if (p->sid == -1) return;
	ArrayInsert(array, p->lc);
	array[i] = p->sid; i++;
	ArrayInsert(array, p->rc);
}

void ArrayMerge(int *achilleas, int *patroklos, int *total, int n1, int n2) {
	int i = 0, j = 0, k = 0;
	while (i < n1 && j < n2) {
		if (achilleas[i] <= patroklos[j]) {
			total[k] = achilleas[i];
			i++; k++;
		}
		else {
			total[k] = patroklos[j];
			j++; k++;
		}
	}
	/* an teleiwsan oi stratiwtes tou patroklou alla oxi tou axillea */
	while (i < n1) {
		total[k] = achilleas[i];
		i++; k++;
	}
	/* an teleiwsan oi stratiwtes tou axillea alla oxi tou patroklou */
	while (j < n2) {
		total[k] = patroklos[j];
		j++; k++;
	}
}

struct TREE_soldier* ArrayToTree(int *total, int start, int end, struct general *G2, struct TREE_soldier *parent) {
	if (start > end) return G2->soldiers_S;
	int middle = (start + end) / 2;

	struct TREE_soldier *R;
	R = (struct TREE_soldier *)malloc(sizeof(struct TREE_soldier));
	if (R == 0) {
		printf("ERROR: Out of memory\n");
		return NULL;
	}
	R->sid = total[middle];
	R->p = parent;

	/* kataskevazw anadromika to aristero ypodentro kai to kanw aristero paidi ths rizas */
	R->lc = ArrayToTree(total, start, middle - 1, G2, R);
	/* omoiws gia to deksio ypodentro */
	R->rc = ArrayToTree(total, middle + 1, end, G2, R);
	return R;
}

/**
 * @brief General resigns from battlefield
 *
 * @param gid1 The id of general that resigns
 * @param gid2 The id of the general that will
 *
 * @return 1 on success
 *         0 on failure
 */
int general_resign(int gid1, int gid2) {

	struct general *G1 = find_general(gid1);
	struct general *G2 = find_general(gid2);
	int n1 = G1->soldiers_no;
	int n2 = G2->soldiers_no;
	int achilleas[n1], patroklos[n2];
	int total[n1 + n2];

	ArrayInsert(achilleas, G1->soldiers_R);
	i = 0; // gia na ksanaksekinisei apo tin arxi (i global)
	ArrayInsert(patroklos, G2->soldiers_R);

	ArrayMerge(achilleas, patroklos, total, n1, n2);

	/* ftiaxnw to teliko dentro kai to dinw ston patroklo */
	G2->soldiers_R = ArrayToTree(total, 0, n1 + n2 - 1, G2, NULL);
	G2->soldiers_no += n1;

	/* svinw ton achillea apo tous generals */
	struct general *g = generals_list, *prev = NULL;
	while (g != G1) {
		prev = g;
		g = g->next;
	}
	if (prev != NULL) prev->next = g->next;
	else generals_list = generals_list->next;

	/* allazw ta gid tou G1 sto registration */
	int i; struct soldier *tmp;
	for (i = 0; i < N; i++) {
		tmp = registration_hashtable[i];
		while (tmp != NULL) {
			if (tmp->gid == gid1) tmp->gid = gid2;
			tmp = tmp->next;
		}
	}
	print_generals();
	return 1;
}

struct TREE_soldier* minValue(struct TREE_soldier *node, struct TREE_soldier *sentinel) {
	struct TREE_soldier *n = node;
	while (n->lc != sentinel)
		n = n->lc;
	return n;
}
struct TREE_soldier* inOrderSuccessor(struct TREE_soldier *x, struct TREE_soldier *sentinel) {
	if (x->rc != sentinel)
		return minValue(x->rc, sentinel);
	else {
		struct TREE_soldier *y = x->p;
		while (y != NULL && x == y->rc) {
			x = y;
			y = y->p;
		}
		return y;
	}
}
struct TREE_soldier* maxValue(struct TREE_soldier *node, struct TREE_soldier *sentinel) {
	struct TREE_soldier *n = node;
	while (n->rc != sentinel)
		n = n->rc;
	return n;
}
struct TREE_soldier* inOrderPredecessor(struct TREE_soldier* x, struct TREE_soldier *sentinel) {
	if (x->lc != sentinel)
		return maxValue(x->lc, sentinel);
	else {
		struct TREE_soldier* y = x->p;
		while (y != NULL && x == y->lc) {
			x = y;
			y = y->p;
		}
		return y;
	}
}
void insertCombat(struct TREE_soldier *tmp, struct general *g) {

	struct c_soldier *c = (struct c_soldier*)malloc(sizeof(struct c_soldier));
	if (c == 0) {
		printf("ERROR: Out of memory\n");
		return;
	}
	c->sid = tmp->sid;
	c->gid = g->gid;
	c->alive = 1;
	c->lc = NULL;
	c->rc = NULL;
	c->left_cnt = 0;

	if (my_combat.combat_s == NULL) {
		my_combat.combat_s = c; //tn 1h fora
		my_combat.soldier_cnt++;
	}
	else {
		struct c_soldier *t = my_combat.combat_s;
		struct c_soldier *prev;
		while (t != NULL) {
			prev = t;
			if (c->sid < t->sid) {
				t->left_cnt++;
				t = t->lc;
			}
			else t = t->rc;
		}
		if (c->sid < prev->sid) prev->lc = c;
		else prev->rc = c;
		my_combat.soldier_cnt++;
	}
}

void print_combat_tree(struct c_soldier *p) {
	if (p == NULL) return;
	print_combat_tree(p->lc);
	printf("<%d> ", p->sid);
	print_combat_tree(p->rc);
}

/**
 * @brief Prepare for battle
 *
 * @param gid1 The id of the first general
 * @param gid2 The id of the second general
 *
 * @return 1 on success
 *         0 on failure
 */
int prepare_battle(int gid1, int gid2) {

	struct general *general1 = find_general(gid1);
	struct general *general2 = find_general(gid2);

	struct TREE_soldier *tmp1 = minValue(general1->soldiers_R, general1->soldiers_S);
	struct TREE_soldier *tmp2 = maxValue(general2->soldiers_R, general2->soldiers_S);

	while (tmp1 != NULL && tmp2 != NULL) {
		insertCombat(tmp1, general1);
		insertCombat(tmp2, general2);
		tmp1 = inOrderSuccessor(tmp1, general1->soldiers_S);
		tmp2 = inOrderPredecessor(tmp2, general2->soldiers_S);
	}
	while (tmp1 != NULL) {
		insertCombat(tmp1, general1);
		tmp1 = inOrderSuccessor(tmp1, general1->soldiers_S);
	}
	while (tmp2 != NULL) {
		insertCombat(tmp2, general2);
		tmp2 = inOrderPredecessor(tmp2, general2->soldiers_S);
	}
	general1->combats_no++;
	general2->combats_no++;

	printf("\tCombat soldiers: ");
	print_combat_tree(my_combat.combat_s);
	printf("\nDONE\n");
	return 1;
}

int toDie;
void PostOrder(struct c_soldier *p) {
	if (p == NULL) return;
	if (toDie != 0) {
		toDie--;
		PostOrder(p->lc);
		PostOrder(p->rc);
		p->alive = 0;
	}
}

void print_alives(struct c_soldier *p) {
	if (p == NULL) return;
	print_alives(p->lc);
	printf("<%d:%d> ", p->sid, p->alive);
	print_alives(p->rc);
}

/**
 * @brief Battle
 *
 * @param god_favor
 * @param bit_stream A sequence of bits
 * @param bitsno The number of bits in the sequence
 *
 * @return 1 on success
 *         0 on failure
 */
int battle(int god_favor, char *bit_stream, int bitsno) {

	if (god_favor == 0) {
		toDie = 0.4 * my_combat.soldier_cnt;
		PostOrder(my_combat.combat_s);
		toDie = 0;
	}
	else if (god_favor == 1) {
		struct c_soldier *c = my_combat.combat_s;
		int i = 0;
		while (c != NULL) {
			c->alive = 0;
			if (bit_stream[i] == '0' && c->lc != NULL) {
				c = c->lc; i++;
			}
			else if (bit_stream[i] == '1' && c->rc != NULL) {
				c = c->rc; i++;
			}
			else break;
			if (i > bitsno) i = 0;
		}
	}
	else printf("Wrong input\n");
	printf("\tCombat soldiers: ");
	print_alives(my_combat.combat_s);
	printf("\nDONE\n");
	return 1;
}

void delete_from_general(int gid, int sid) {
	struct general *g = find_general(gid);
	struct TREE_soldier *t = g->soldiers_R;

	g->soldiers_S->sid = sid;

	/* anti na xrhsimopoihsw voithitiko deikti prev
	vriskw katefthian to sid me ton komvo frouro */
	while (t->sid != g->soldiers_S->sid) {
		if (sid < t->sid) t = t->lc;
		else t = t->rc;
	}
	g->soldiers_S->sid = -1;

	struct TREE_soldier *y, *x;
	if (t->lc == g->soldiers_S || t->rc == g->soldiers_S)
		y = t;
	else y = inOrderSuccessor(t, g->soldiers_S);

	if (y->lc != g->soldiers_S) x = y->lc;
	else x = y->rc;
	if (x != g->soldiers_S) x->p = y->p;

	if (y->p == NULL) return; //diagrafi rizas
	else if (y == y->p->lc) y->p->lc = x;
	else y->p->rc = x;
	if (y != t) t->sid = y->sid;
}

void delete_from_hashtable(int sid) {
	int bucket = sid % N;
	struct soldier *r = registration_hashtable[bucket];
	struct soldier *prev = r;
	while (r->sid != sid) {
		prev = r;
		r = r->next;
	}
	if (prev != r) prev->next = r->next;
	else registration_hashtable[bucket] = r->next;
	r->next = NULL; free(r);
}

struct c_soldier* Successor(struct c_soldier *c) {
	if (c->rc != NULL) {
		struct c_soldier *y = c->rc;
		if (y->lc == NULL) return y;
		else {
			while (y->lc != NULL)
				y = y->lc;
			return y;
		}
	}
	else return NULL;
}

void delete_from_combat(struct c_soldier *c, struct c_soldier *prev) {

	struct c_soldier *y, *x, *tmp = c;

	while (tmp != NULL && tmp->alive == 0) {

		delete_from_general(tmp->gid, tmp->sid);
		delete_from_hashtable(tmp->sid);

		/* o komvos pou thelo na diagrapso exei 0 h 1 paidi */
		if (tmp->lc == NULL || tmp->rc == NULL) {
			y = tmp;
			if (y->lc != NULL) x = y->lc;
			else x = y->rc;
			if (prev == NULL) {
				my_combat.combat_s = x; //diagrafi rizas
			}
			else if (y == prev->lc) prev->lc = x;
			else prev->rc = x;
			break;
		} else { /* 2 paidia */
			y = tmp->rc;
			while (y->lc != NULL) {
				tmp = y;
				y = y->lc;
			    tmp->left_cnt--;
			}
			x = y->rc;
			if (tmp == NULL) my_combat.combat_s = x; //diagrafi rizas
			else if (y == tmp->lc) tmp->lc = x;
			else tmp->rc = x;
			tmp->sid = y->sid;
			tmp->gid = y->gid;
			tmp->alive = y->alive;
		}
	}
}

void printcombat(struct c_soldier *c) {
	if (c == NULL) return;
	printcombat(c->lc);
	printf("%d ", c->sid);
	printcombat(c->rc);
}

void DeleteSoldiers(struct c_soldier *c, struct c_soldier *prev) {
	if (c == NULL) return;
	DeleteSoldiers(c->lc, c);
	DeleteSoldiers(c->rc, c);
	if (c->alive == 0) {
		delete_from_combat(c, prev);
	}
}

/**
 * @brief Cease fire
 *
 * @return 1 on success
 *         0 on failure
 */
int cease_fire() {
	struct c_soldier *c = my_combat.combat_s;

	DeleteSoldiers(c, NULL);

	printf("\tCombat tree: ");
	printcombat(my_combat.combat_s);

	printf("\n\n\tGENERALS LIST:");
	struct general *g = generals_list;
	while (g != NULL) {
		printf("\n\t<%d>: ", g->gid);
		if (g->soldiers_R != NULL)
			InOrder(g->soldiers_R); //Visit: print
		g = g->next;
	}
	printf("\n");

	int i; struct soldier *tmp;
	printf("\n\tSOLDIERS HASH TABLE:\n\t");
	for (i = 0; i < N; i++) {
		tmp = registration_hashtable[i];
		while (tmp != NULL) {
			printf("<%d:%d> ", tmp->sid, tmp->gid);
			tmp = tmp->next;
		}
		printf("\n\t");
	}
	printf("\nDONE\n");
	return 1;
}

/**
 * @brief Print general's soldiers
 *
 * @param gid The id of the general
 *
 * @return 1 on success
 *         0 on failure
 */
int print_generals_soldiers(int gid) {
	struct general *g = find_general(gid);
	printf("\tSoldier tree = ");
	if (g->soldiers_R != g->soldiers_S)
		InOrder(g->soldiers_R);
	printf("\nDONE\n");
	return 1;
}

/**
 * @brief Print generals
 *
 * @return 1 on success
 *         0 on failure
 */
int print_generals() {
	printf("\tGENERALS:");
	struct general *g = generals_list;
	while (g != NULL) {
		printf("\n\t<%d>: ", g->gid);
		if (g->soldiers_R != NULL)
			InOrder(g->soldiers_R);
		g = g->next;
	}
	printf("\nDONE\n");
	return 1;
}

/**
 * @brief Print registration hashtable
 *
 * @return 1 on success
 *         0 on failure
 */
int print_registration_hashtable() {
	printf("\tRegistration list = ");
	int i; struct soldier *tmp;
	for (i = 0; i < N; i++) {
		tmp = registration_hashtable[i];
		while (tmp != NULL) {
			printf("<%d:%d> ", tmp->sid, tmp->gid);
			tmp = tmp->next;
		}
	}
	printf("\nDONE\n");
	return 1;
}

/**
 * @brief Free resources
 *
 * @return 1 on success
 *         0 on failure
 */
int free_world() {
	return 1;
}

/**
 * @brief The main function
 *
 * @param argc Number of arguments
 * @param argv Argument vector
 *
 * @return 0 on success
 *         1 on failure
 */
int main(int argc, char** argv)
{
	FILE *fin = NULL;
	char buff[BUFFER_SIZE], event;

	/* Check command buff arguments */
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <input_file> \n", argv[0]);
		return EXIT_FAILURE;
	}

	/* Open input file */
	if ((fin = fopen(argv[1], "r")) == NULL) {
		fprintf(stderr, "\n Could not open file: %s\n", argv[1]);
		perror("Opening test file\n");
		return EXIT_FAILURE;
	}

	/* Read input file buff-by-buff and handle the events */
	while (fgets(buff, BUFFER_SIZE, fin)) {

		DPRINT("\n>>> Event: %s", buff);

		switch (buff[0]) {

			/* Comment */
		case '#':
			break;

			/* Maximum soldiers */
		case '0':
		{
			sscanf(buff, "%c %d", &event, &max_soldiers_g);
			DPRINT("%c %d\n", event, max_soldiers_g);

			initialize();

			break;
		}

		/* Register soldier
		 * R <sid> <gid> */
		case 'R':
		{
			int sid, gid;

			sscanf(buff, "%c %d %d", &event, &sid, &gid);
			DPRINT("%c %d %d\n", event, sid, gid);

			if (register_soldier(sid, gid)) {
				DPRINT("R %d %d succeeded\n", sid, gid);
			}
			else {
				fprintf(stderr, "R %d %d failed\n", sid, gid);
			}

			break;
		}

		/* General or king joins the battlefield
		 * G <gid> */
		case 'G':
		{
			int gid;

			sscanf(buff, "%c %d", &event, &gid);
			DPRINT("%c %d\n", event, gid);

			if (add_general(gid)) {
				DPRINT("%c %d succeeded\n", event, gid);
			}
			else {
				fprintf(stderr, "%c %d failed\n", event, gid);
			}

			break;
		}

		/* Distribute soldier
		 * D */
		case 'D':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (distribute()) {
				DPRINT("%c succeeded\n", event);
			}
			else {
				fprintf(stderr, "%c failed\n", event);
			}

			break;
		}

		/* General resigns from battle
		 * M <gid1> <gid2> */
		case 'M':
		{
			int gid1, gid2;

			sscanf(buff, "%c %d %d", &event, &gid1, &gid2);
			DPRINT("%c %d %d\n", event, gid1, gid2);

			if (general_resign(gid1, gid2)) {
				DPRINT("%c %d %d succeeded\n", event, gid1, gid2);
			}
			else {
				fprintf(stderr, "%c %d %d failed\n", event, gid1, gid2);
			}

			break;
		}

		/* Prepare for battle
		 * P <gid1> <gid2> */
		case 'P':
		{
			int gid1, gid2;
			sscanf(buff, "%c %d %d", &event, &gid1, &gid2);
			DPRINT("%c %d %d\n", event, gid1, gid2);

			if (prepare_battle(gid1, gid2)) {
				DPRINT("%c %d %d succeeded\n", event, gid1, gid2);
			}
			else {
				fprintf(stderr, "%c %d %d failed\n", event, gid1, gid2);
			}

			break;
		}

		/* Battle
		 * B <god_favor> <bit_stream> */
		case 'B':
		{
			int god_favor;
			char bit_stream[9];

			sscanf(buff, "%c %d %s\n", &event, &god_favor, bit_stream);
			DPRINT("%c %d %s\n", event, god_favor, bit_stream);

			if (battle(god_favor, bit_stream, 8)) {
				DPRINT("%c %d %s succeeded\n", event, god_favor, bit_stream);
			}
			else {
				fprintf(stderr, "%c %d %s failed\n", event, god_favor, bit_stream);
			}

			break;
		}

		/* Cease fire
		 * U */
		case 'U':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (cease_fire()) {
				DPRINT("%c succeeded\n", event);
			}
			else {
				fprintf(stderr, "%c failed\n", event);
			}

			break;
		}

		/* Print general's soldiers
		 * W <gid> */
		case 'W':
		{
			int gid;

			sscanf(buff, "%c %d", &event, &gid);
			DPRINT("%c %d\n", event, gid);

			if (print_generals_soldiers(gid)) {
				DPRINT("%c %d succeeded\n", event, gid);
			}
			else {
				fprintf(stderr, "%c %d failed\n", event, gid);
			}

			break;
		}

		/* Print generals
		 * X */
		case 'X':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (print_generals()) {
				DPRINT("%c succeeded\n", event);
			}
			else {
				fprintf(stderr, "%c failed\n", event);
			}

			break;
		}

		/* Print registration hashtable
		 * Y */
		case 'Y':
		{
			sscanf(buff, "%c", &event);
			DPRINT("%c\n", event);

			if (print_registration_hashtable()) {
				DPRINT("%c succeeded\n", event);
			}
			else {
				fprintf(stderr, "%c failed\n", event);
			}

			break;
		}

		/* Empty line */
		case '\n':
			break;

			/* Ignore everything else */
		default:
			DPRINT("Ignoring buff: %s \n", buff);

			break;
		}
	}
	free_world();
	return (EXIT_SUCCESS);
}
